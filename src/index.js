'use strict';

/**
 * Return a fail-middleware catches Mongo duplicate errors in the pipeline.
 * @param {HandledError} handledError
 * @returns {Function}
 */
function catchMongoDuplicateError(handledError) {
  return (err, next) => {
    const matched = err.name === 'MongoError' && err.code === 11000;
    matched ? next(null, handledError) : next(null, err);
  };
}

module.exports = catchMongoDuplicateError;
